FROM debian:bullseye

SHELL ["/bin/bash", "-c"]

COPY /scripts /scripts

ENV STEAM_USERNAME=anonymous
ENV STEAM_PASSWORD=password
ENV CACHE_BUILD=false
ENV CACHE=false

ENV PATH "$PATH:/usr/games/:/scripts/:/opt/"

RUN sed -i '/^\([^#].*main\)/s/main/& contrib non-free/' /etc/apt/sources.list && \
    dpkg --add-architecture i386 && \
    apt-get update && \ 
    echo 2 | apt-get install curl steamcmd -y && \
    chmod +x /scripts/*

